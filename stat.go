package main

import (
	"os"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/ai69/getmyip"
	pretty "github.com/jedib0t/go-pretty/progress"
	"go.uber.org/zap"
)

func countDiskFreeSpace() uint64 {
	var (
		stat syscall.Statfs_t
		err  error
		wd   string
	)

	if wd, err = os.Getwd(); err != nil {
		log.Panicw("fail to get current working directory", zap.Error(err))
	}

	if err = syscall.Statfs(wd, &stat); err != nil {
		log.Panicw("fail to call Statfs", zap.Error(err))
	}

	return stat.Bavail * uint64(stat.Bsize)
}

func size2str(s uint64) string {
	return pretty.FormatBytes(int64(s))
}

func checkSystemStat() {
	host, _ := os.Hostname()
	pwd, _ := os.Getwd()
	diskFreeSize := countDiskFreeSpace()
	cpuCores := runtime.NumCPU()
	publicIp, _ := getmyip.GetPublicIP()
	outboundIp, _ := getmyip.GetOutboundIP()

	log.Infow("check system status",
		"host", host,
		"pwd", pwd,
		"public_ip", publicIp,
		"outbound_ip", outboundIp,
		"free_disk", size2str(diskFreeSize),
		"cpu_core", cpuCores,
	)
}

func checkMemoryStat() {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)

	log.Infow("check memory status",
		"total_alloc", size2str(m.TotalAlloc),
		"heap_alloc", size2str(m.HeapAlloc),
		"sys_mem", size2str(m.Sys),
		"gc_count", m.NumGC)
}

func autoCheck(intervalSec int, check func()) {
	interval := time.Duration(intervalSec) * time.Second
	ticker := time.NewTicker(interval)
	defer ticker.Stop()
	for {
		go func() {
			check()
		}()
		<-ticker.C
	}
}
