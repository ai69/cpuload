package main

import (
	"flag"
	"fmt"
	"runtime"
	"sync"
	"time"
)

var (
	useLogger bool
	size      int
	worker    int
	round     int
	times     int
)

func init() {
	defaultWorker := runtime.NumCPU() / 3
	if defaultWorker <= 0 {
		defaultWorker = 1
	}

	flag.BoolVar(&useLogger, "log", false, "enable logger instead of progress")
	flag.IntVar(&size, "size", 1024*1024*2, "size of array")
	flag.IntVar(&worker, "worker", defaultWorker, "number of workers")
	flag.IntVar(&round, "round", 1024, "number of rounds for every run")
	flag.IntVar(&times, "times", 0, "times times, 0 for infinite")
	flag.Parse()

	if useLogger {
		setVerboseLogger()
	} else {
		setQuietLogger()
	}
	log.Infow("task loaded",
		"log", useLogger,
		"size", size2str(uint64(size)),
		"worker_num", worker,
		"round_num", round,
		"repeat_times", times)
}

func main() {
	if useLogger {
		checkSystemStat()
		checkMemoryStat()
	}

	for r := 1; r <= times || times == 0; r++ {
		start := time.Now()
		if useLogger {
			log.Infow("round starts", "round", r, "total", times)
		} else {
			fmt.Printf("====== Round: %2d/%2d ======\n", r, times)
			setProgressWriter(worker * 2)
		}

		var wg sync.WaitGroup
		for w := 1; w <= worker; w++ {
			wg.Add(1)
			go workLoad(w, &wg)
		}

		if useLogger {
			go autoCheck(10, checkMemoryStat)
		}

		wg.Wait()
		time.Sleep(500 * time.Millisecond)

		if useLogger {
			log.Infow("round starts", "round", r, "time_cost", time.Now().Sub(start))
		} else {
			fmt.Println()
		}
	}
}
