# cpuload

**cpuload** is a cli tool to create mock CPU workload.

## install

```bash
go get bitbucket.org/ai69/cpuload
```

## usage

```bash
cpuload
```
