package main

import (
	"time"

	"github.com/jedib0t/go-pretty/progress"
)

var (
	progressWriter progress.Writer
)

func setProgressWriter(count int) {
	pw := progress.NewWriter()
	pw.SetAutoStop(true)
	pw.SetTrackerLength(30)
	pw.ShowOverallTracker(true)
	pw.ShowTime(true)
	pw.ShowTracker(true)
	pw.ShowValue(true)
	pw.SetMessageWidth(20)
	pw.SetNumTrackersExpected(count)
	pw.SetSortBy(progress.SortByMessage)
	pw.SetStyle(progress.StyleCircle)
	pw.SetTrackerPosition(progress.PositionRight)
	pw.SetUpdateFrequency(time.Millisecond * 100)
	pw.Style().Colors = progress.StyleColorsExample
	pw.Style().Options.PercentFormat = "%4.1f%%"
	go pw.Render()

	progressWriter = pw
}

func getProgressTracker(msg string, total int) *progress.Tracker {
	return &progress.Tracker{
		Message: msg,
		Total:   int64(total),
		Units:   progress.UnitsDefault,
	}
}
