module bitbucket.org/ai69/cpuload

go 1.16

require (
	bitbucket.org/ai69/getmyip v0.0.9
	github.com/1set/gut v0.0.0-20201117175203-a82363231997
	github.com/go-openapi/strfmt v0.21.3 // indirect
	github.com/jedib0t/go-pretty v4.3.0+incompatible
	github.com/mattn/go-runewidth v0.0.14 // indirect
	go.uber.org/zap v1.23.0
)
