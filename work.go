package main

import (
	"fmt"
	"sync"
	"time"

	"github.com/1set/gut/yrand"
	"github.com/jedib0t/go-pretty/progress"
	"go.uber.org/zap"
)

func workLoad(id int, wg *sync.WaitGroup) {
	defer wg.Done()
	var (
		track    *progress.Tracker
		logger   *zap.SugaredLogger
		useTrack = !useLogger
		start    = time.Now()
	)

	if useLogger {
		logger = log.With("id", id)
		logger.Debugw("worker starts", "size", size, "round", round)
		defer logger.Sync()
	} else {
		track = getProgressTracker(fmt.Sprintf("worker%3d initialize", id), size)
		progressWriter.AppendTracker(track)
	}

	arrFloat := make([]float64, size, size)
	arrInt := make([]int64, size, size)
	for i := 0; i < size; i++ {
		arrFloat[i] = randFloat()
		arrInt[i] = randInt()

		if useTrack {
			track.Increment(1)
		}
	}

	if useLogger {
		logger.Debugw("array of numbers initializes")
	} else {
		track.MarkAsDone()
		track = getProgressTracker(fmt.Sprintf("worker%3d compute", id), size*round)
		progressWriter.AppendTracker(track)
	}

	for t := 1; t <= round; t++ {
		rf := randFloat()
		ri := randInt()
		for i := 0; i < size; i++ {
			arrFloat[i] *= rf
			arrInt[i] *= ri

			if useTrack && (i%1000 == 0) && i > 0 {
				track.Increment(1000)
			}
		}
	}

	if useLogger {
		logger.Debugw("worker ends", "time_cost", time.Now().Sub(start))
	} else {
		track.MarkAsDone()
	}
}

func randFloat() float64 {
	rf, _ := yrand.Float64()
	return rf
}

func randInt() int64 {
	ri, _ := yrand.Int64Range(2, 100000000)
	return ri
}
